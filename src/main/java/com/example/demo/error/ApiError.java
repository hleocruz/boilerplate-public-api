package com.example.demo.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {
	
	@JsonProperty("code")
	private Long code;
	
	@JsonProperty("detailedMessage")
	private String detailedMessage;
	
	@JsonProperty("helpUrl")
	private String helpUrl;
	
	@JsonProperty("message")
	private String message;

}
