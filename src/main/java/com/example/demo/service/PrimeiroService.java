package com.example.demo.service;

import com.example.demo.client.PrimeiroClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrimeiroService {

    @Autowired
    private PrimeiroClient client;

    public Object primeiro() {
        return client.primeiro();
    }
}
