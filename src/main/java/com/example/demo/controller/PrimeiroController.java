package com.example.demo.controller;

import com.example.demo.service.PrimeiroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("primeiro")
@RestController
public class PrimeiroController {

    @Autowired
    private PrimeiroService service;

    @GetMapping
    public Object primeiro() {
        return service.primeiro();
    }
}
