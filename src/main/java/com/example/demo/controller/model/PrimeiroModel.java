package com.example.demo.controller.model;

public class PrimeiroModel {

    private String texto;

    public PrimeiroModel(String texto) {
        this.texto = texto;
    }

    public PrimeiroModel() {
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
