package com.example.demo.config;

import com.example.demo.client.PrimeiroClient;
import com.example.demo.error.ApiError;
import com.example.demo.error.ErrorResponseException;
import com.example.demo.error.ExceptionEnum;
import com.example.demo.error.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.Request;
import feign.RequestInterceptor;
import feign.auth.BasicAuthRequestInterceptor;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.optionals.OptionalDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;

@Configuration
public class OpenFeignConfig {
	
	Logger logger = LoggerFactory.getLogger(OpenFeignConfig.class);
    
    @Value("${url}")
    private String url;
    
    @Value("${username}")
    private String username;
    
    @Value("${password}")
    private String password;
        
    @Autowired
    private ObjectMapper mapper;

    @Bean
    @Profile("!prod")
    public PrimeiroClient primeiroClient() {
    	return getClientForAPI(PrimeiroClient.class, url);
    }
    
    protected <T> T getClientForAPI(Class<T> apiClass, String url, RequestInterceptor... interceptors) {
        Feign.Builder builder = Feign.builder()
        		.errorDecoder(decoder())
                .encoder(new JacksonEncoder())
                .decoder(new OptionalDecoder(new JacksonDecoder()))
                .options(new Request.Options((180 * 1000), (60 * 1000)));
        for (RequestInterceptor interceptor : interceptors){
            builder.requestInterceptor(interceptor);
        }
        return builder.target(apiClass, url);
    }
    
    public RequestInterceptor authorizationHeaderRequestInterceptor() {
        return new BasicAuthRequestInterceptor(username, password);
    }

    private ErrorDecoder decoder() {
		return (methodKey, response) -> {
			ApiError error = null;
			try {
				
				logger.info("decoder.resquest: \n" + response.request());
				logger.info("decoder.status: " + response.status());
				logger.info("decoder.reason: " + response.reason());
				logger.info("decoder.header: " + response.headers());
				
				error = mapper.readValue(response.body().asInputStream(), ApiError.class);
			} catch (Exception e) {
				throw new ErrorResponseException(
						Response.builder().code(ExceptionEnum.INTERNAL_SERVER_ERROR.getId())
								.description(ExceptionEnum.INTERNAL_SERVER_ERROR.getDescription())
								.message(ExceptionEnum.INTERNAL_SERVER_ERROR.getDescription()).build(),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			throw new ErrorResponseException(Response.builder().code(error.getCode().intValue())
					.description(error.getDetailedMessage()).message(error.getMessage()).build(), HttpStatus.valueOf(response.status()));
		};
	}
}
