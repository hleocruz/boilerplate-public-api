package com.example.demo.client;

import com.example.demo.controller.model.PrimeiroModel;
import feign.RequestLine;
import org.springframework.stereotype.Component;

@Component
public interface PrimeiroClient {

    @RequestLine("GET /some/thing")
    PrimeiroModel primeiro();
}
