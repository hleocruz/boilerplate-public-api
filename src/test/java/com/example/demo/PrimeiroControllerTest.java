package com.example.demo;

import com.example.demo.config.WireMockConfig;
import com.example.demo.controller.model.PrimeiroModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PrimeiroControllerTest extends WireMockConfig {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void contextLoads() throws Exception {

		final MvcResult primeiro = this.mockMvc.perform(get("/primeiro")).andExpect(status().isOk()).andReturn();
		final String json = primeiro.getResponse().getContentAsString();
		final PrimeiroModel response = objectMapper.readValue(json, PrimeiroModel.class);

		Assert.assertEquals("Hello world!", response.getTexto());

	}
}
