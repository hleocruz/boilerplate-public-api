package com.example.demo.config;

import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import org.junit.ClassRule;
import org.junit.Rule;

public class WireMockConfig {


    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(
            WireMockConfiguration.options()
                    .port(8082)
                    .notifier(new ConsoleNotifier(true))
    );

    @Rule
    public WireMockClassRule instanceRule = wireMockRule;


}
